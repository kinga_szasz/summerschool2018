// More exercises

#include "Exercises_Pointers.h"
#include "Util.h"
#include <iostream>

// Exercise:
// Use pointer arithmetics to print the double value as different types:
// Print it as ints (how many it fits)
// Print it as chars (how many it fits)
void PointerArithmetics()
{
    double c = 2.06843e+272;;
}

// Exercise:
// the Key struct has two members from which only one is accessible
// Expose the secretKey here, note you don't have access to the globalKey.secretKey;
extern Ex::Key globalKey;
const char* Ex::ExposeSecretKey()
{   
    return "";
}

// Exercise:
// 1) Convert the input char* parameter ( you should use a switch statement) 
//          -1 if input is invalid
//          0 for "opt1"
//          1 for "opt2"
//          2 for "opt3"
//          3 if otherwise
// 
int Ex::SwitchChar(char* param)
{
    int o = 0;
    switch (o)
    {
    case 'opt1': return 0;
    case 'opt2': return 1;
    case 'opt3': return 2;
    default: return 3;
    }
}

// Exercise (c-string):
// this should return the number of occurrences of c in str or -1 in case of failure
int Ex::CountChars(const char* str, char c)
{
    return -1;
}

// Exercise:
// Convert a string to an integer, return 0 if success.
// Ignore spaces. A negative number is valid only if "-" is the first non-space char.
int Ex::Str2Num(const char* str, int& num)
{
    return -1;
}

// Exercise: (dyn mem), use only dynamic memory to implement the following function
// 1. Ask user for the number of numbers and save it
// 2. Ask user for each number and save them
// 3. Display the numbers separated by space
// 4. Display the max, min, and the mean for the given numbers
// 5. Don't forget to cleanup memory.
void Ex::Numbers()
{

}

// Exercise (c-string + dyn mem):
// Implement CopyRange: should copy from source to dest a range of characters starting minIndex and ending just before maxIndex
// dest should be null terminated
// returns 0 if success or different if bad parameters or failure.
// the pointer should be set to nullptr if something is not valid
int Ex::CopyRange(char*& dest, const char* source, size_t minIndex, size_t maxIndex)
{
    return 0;
}

// Exercise:
// Write a function that given a string of words will return an array of words
// a word is considered a series of consecutive letters that do not include spaces
// return 0 if success or something different than 0 on failure.
//int SeparateWords(const char* strWithWords, char*** words, size_t* numWords);
int Ex::SeparateWords(const char* strWithWords, char**& words, size_t& numWords)
{
    return 0;
}

// Exercise:
// Change this method to check if the input word is of the required size first
bool Ex::IsValidWord(const char* word, size_t requiredSize)
{
    return Util::WordExistsInDexonline(word);
}

// Exercise:
// Implement this using Util::GenerateRandomLeter to generate a random word
char* Ex::GenerateWord(size_t wordSize)
{
    return nullptr;
}

// Exercise:
// use GenerateWord and IsValidWord to generate a valid word, don't waste memory
char* Ex::GenerateValidWord(size_t wordSize)
{
    return nullptr;
}

// Exercise:
// 1. Request user input for the numbers of words (numWords)
// 2. Request user input for the size of the word (wordSize)
// 3. Generate numWords random words of size wordSize and print them.
// 4. Prompt user to check if the words are valid (if he likes them) : 
//      Show a list like 1: "word1", 2: "word2", 3: "word3", 4: "word4". User should respond with a list of numbers like 2 3. Use cin.getline to read. 
//      If user input is not of the form i1 i2 i3 i4 where i is a word index that we previously showed, then user is reprompted.
//      Use SeparateWords and Str2Num to validate user input and keep the words that user selected.
// 5. Print some senteces with the valid words. (Optional print all permutations of the valid words == all possible sentences)
void Ex::GiveMeSomeWords()
{

}
