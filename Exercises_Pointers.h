// More exercises
#pragma once

namespace Ex
{
    struct Key
    {
        Key(const char* v, const char* s) : publicVersion(v), secretKey(s) {};

        const char* publicVersion;
    private: const char* secretKey;
    };

    // Exposes the secretKey decleared in Exercises_Pointers.cpp
    const char* ExposeSecretKey();

    // uses str as an option in switch to return different return codes.
    int SwitchChar(char* str);
    
    // A word is valid if its size is requiredSize and exists in the dictionary
    bool IsValidWord(const char* word, size_t requiredSize);

    // Generates a random word of a given wordSize
    char* GenerateWord(size_t wordSize);

    // Generates a valid word
    char* GenerateValidWord(size_t wordSize);

    // Counts the number of occurrences of c in str, returns -1 in case of failure
    int CountChars(const char* str, char c);

    // Converts a c-string to an integer, return != 0 on failure
    int Str2Num(const char* str, int& num);

    // exercise with dynamic memory
    void Numbers();

    // copies a range of characters from source to dest
    // allocates required memory for dest, caller should handle it
    int CopyRange(char*& dest, const char* source, size_t minIndex, size_t maxIndex);

    // given a string with words (separated by space) the method returns an array of words
    // caller is responsible to release the allocated memory
    // returns -1 in case of failure
    int SeparateWords(const char* strWithWords, char**& words, size_t& size);

    // playing with words with user interaction
    void GiveMeSomeWords();
}

