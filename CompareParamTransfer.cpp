#include <ctime>
#include <iostream>
#include "CompareParamTransfer.h"
#include "Util.h"

struct Arr
{
    double arr[1000];
};

void TestValue(Arr a, Arr b)
{
    a.arr[10] = 1;
    b.arr[10] = 1;
}

void TestRef(Arr& a, Arr& b)
{
    a.arr[10] = 1;
    b.arr[10] = 1;
}

void TestPointer(Arr* a, Arr* b)
{
    a->arr[10] = 1;
    b->arr[10] = 1;
}

void Ex::CompareRefVsValueTransfer()
{
    const size_t numRuns = 0xF0000;
    Arr a, b;
    double transferByValue = Util::MeasureTime<numRuns>(TestValue, a, b);
    double transferByRef = Util::MeasureTime<numRuns>(TestRef, a, b);
    double transferByPointer = Util::MeasureTime<numRuns>(TestPointer, &a, &b);
    std::cout << "By Value:" << transferByValue << "\nBy Ref:" << transferByRef << "\nBy Pointer:" << transferByPointer;
}