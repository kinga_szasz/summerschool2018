#include <iostream>

namespace
{
    void PrintInt(int i)
    {
        std::cout<<i<<"\n";
    }
    // function as function parameter
    void CallFunc(void (*p)(int), int i)
    {
        p(i);    
    }
}

namespace Ex
{
    
    void PointerToFunction()
    {
        void (*p)(int) = PrintInt; // function pointer declaration and initialization
        p(3);
        p(4);
        CallFunc(p,33);
        CallFunc(PrintInt,44);
    }

    // Declare Operation as a function that takes two double parameters and returns a double;
    typedef void* Operation; 

    // Implement a function that receives an Operation as input and return the result applied on param1 and param2
    double GetResult(Operation operation, double param1, double param2)
    {
        return 0;
    }
}
